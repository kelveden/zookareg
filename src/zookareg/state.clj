(ns zookareg.state
  "Holds the state for Zookareg and ensures it's not unloaded."
  (:require [clojure.tools.namespace.repl :as repl]))

(repl/disable-reload!)

(def state (atom nil))
