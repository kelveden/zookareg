(ns zookareg.utils
  (:require [clojure.walk :as walk])
  (:import [java.io File FileOutputStream]
           java.net.ServerSocket
           java.util.Properties))

(defn ^Properties m->properties [m]
  (let [ps (Properties.)]
    (doseq [[k v] m] (.setProperty ps (name k) (str v)))
    ps))

(defn ->available-port []
  (with-open [s (ServerSocket. 0)]
    (.setReuseAddress s true)
    (.getLocalPort s)))

(defn ^String store-properties
  [^Properties properties ^File file]
  (with-open [out (FileOutputStream. file)]
    (.store properties
            out
            "Genereated by zookareg"))
  (.getAbsolutePath file))

(defn disqualify-keys [m]
  (walk/postwalk #(if (keyword? %) (keyword (name %)) %)
                 m))
