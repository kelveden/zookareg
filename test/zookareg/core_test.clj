(ns zookareg.core-test
  (:require [clojure.test :refer :all]
            [zookareg.core :as sut]
            [zookareg.state :as zookareg.state]))

(defn- trim-kafka-log-dir
  "take 5 first chars of Kafka's log.dir, since it is an available temp dir,
  with junk attached at the end."
  [c]
  (update-in c [:zookareg.kafka/server :config :log.dir] (partial take 5)))

(defn state-matches? [expected-config]
  (is (= (trim-kafka-log-dir expected-config)
         (trim-kafka-log-dir (:config @zookareg.state/state))))
  (is (:system @zookareg.state/state)))

(defn state-lifecycle-works?-fn
  "test that the stored ig-config is derived from the provided zookareg config
  and that state and config are reset after run"
  [config= f]
  (f)
  (state-matches? config=)
  (sut/halt-zookareg!)
  (is (nil? @zookareg.state/state)))

(defmacro state-lifecycle-works? [config= & body]
  `(state-lifecycle-works?-fn ~config= (fn [] ~@body)))

(deftest init-zookareg-test
  (testing "uses default config by default"
    (state-lifecycle-works?
     (sut/read-default-config)
     (sut/init-zookareg))))

(defn- set-kafka-auto-commit-enable=false
  [config]
  (assoc-in config [:zookareg.kafka/server :config :auto.commit.enable ] false))

(deftest with-zookareg-fn-test
  (testing "using defaults"
    (sut/with-zookareg-fn
      #(state-matches? (sut/read-default-config))))
  (testing "specifying config"
    ;; TODO better test that checks changes have gone through
    (let [c (-> (sut/read-default-config)
                set-kafka-auto-commit-enable=false)]
      (sut/with-zookareg-fn c
        #(state-matches? c))))
  (testing "blows up if config is invalid"
    (is (thrown? AssertionError
                 (sut/with-zookareg-fn :foo (fn []))))))

(deftest with-zookareg-test
  (testing "specifying config"
    ;; TODO better test that checks changes have gone through
    (let [c (-> (sut/read-default-config)
                set-kafka-auto-commit-enable=false)]
      (sut/with-zookareg c
        (state-matches? c))))
  (testing "blows up if config is invalid"
    (is (thrown? AssertionError
                 (sut/with-zookareg :foo)))))
